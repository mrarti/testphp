<?php $__env->startSection('title','Добавление ответов'); ?>
<?php $__env->startSection('maincontent'); ?>
<form name="add_name" id="add_name">  
<h1>Добавление ответов</h1>
<h2><?php echo e($idquest->explanation); ?></h2>
<?php $__currentLoopData = $quest; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $questions): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <label class="my-1 mr-2" for="inlineFormCustomSelectPref"><?php echo e($questions->questiontext); ?></label>
  <div class="alert alert-danger print-error-msg" style="display:none">
  <ul></ul>
  </div>
  <div class="alert alert-success print-success-msg" style="display:none">
  <ul></ul>
  </div>
  <div class="table-responsive">  
      <table class="table table-bordered" id="dynamic_field<?php echo e($questions->standartquestion_id_pk); ?>">  
          <tr>  
              <td><input type="text" name="answertext<?php echo e($questions->standartquestion_id_pk); ?>[]"  name="<?php echo e($questions->standartquestion_id_pk); ?>" placeholder="Введите ответ №1" class="form-control name_list" /></td> 
              <td><div class="form-check">
              <input class="form-check-input" name="true<?php echo e($questions->standartquestion_id_pk); ?>" type="radio" value="1">
              <label class="form-check-label" >Правильный ответ</label>
              </div></td>
              <td><button type="button" name="add<?php echo e($questions->standartquestion_id_pk); ?>" id="add<?php echo e($questions->standartquestion_id_pk); ?>" class="btn btn-success">Добавить ответ</button></td>  
          </tr>  
      </table>

<script type="text/javascript">
    $(document).ready(function(){      
      var i=1;  
    
    

      $('#add<?php echo e($questions->standartquestion_id_pk); ?>').click(function(){  
           i++;  
           $('#dynamic_field<?php echo e($questions->standartquestion_id_pk); ?>').append('<tr id="row'+i+'" class="dynamic-added"><td><input type="text" name="answertext<?php echo e($questions->standartquestion_id_pk); ?>[]" placeholder="Введите ответ №'+i+'" class="form-control name_list" /></td> <td><div class="form-check"><input class="form-check-input" name="true<?php echo e($questions->standartquestion_id_pk); ?>" id="'+i+'" type="radio" value="1"><label class="form-check-label" >Правильный ответ</label></div></td><td><button type="button" name="remove<?php echo e($questions->standartquestion_id_pk); ?>" id="'+i+'" class="btn btn-danger btn_remove">Удалить ответ</button></td></tr>');  
      });  


      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  


      $.ajaxSetup({
        beforeSend: function(xhr, type) {
            if (!type.crossDomain) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
            }
        },
    });

    
 
    });  
</script>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      <input type="button" name="submit" id="submit" class="btn btn-info" value="Далее" />  
  </div>
</div>
</form>
<script>
var postURL = "<?php echo url('quest'); ?>";
   $('#submit').click(function(){            
           $.ajax({  
                url:postURL,  
                method:"POST",  
                data:$('#add_name').serialize(),
                type:'json',
                success:function(data)  
                {
                    if(data.error){
                        printErrorMsg(data.error);
                    }else{
                        i=1;
                        $('.dynamic-added').remove();
                        $('#add_name')[0].reset();
                        $(".print-success-msg").find("ul").html('');
                        $(".print-success-msg").css('display','block');
                        $(".print-error-msg").css('display','none');
                        $(".print-success-msg").find("ul").append('<li>Запись была добавлена</li>');
                    }
                }  
           });  
      });

      
      function printErrorMsg (msg) {
         $(".print-error-msg").find("ul").html('');
         $(".print-error-msg").css('display','block');
         $(".print-success-msg").css('display','none');
         $.each( msg, function( key, value ) {
            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
         });
      } 
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>