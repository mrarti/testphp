<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.welcome');
});

Route::group(['middleware' => ['auth']], function() {
    Route::get("test","AddTestController@addView");
    Route::post("test","AddTestController@addPost");
    Route::get("quest","AddTestController@questView");
    Route::post("quest","AddTestController@questPost");
});
Route::get('/','HomeController@db');
Route::get('/about','HomeController@about');

Auth::routes();