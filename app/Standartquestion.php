<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Standartquestion extends Model
{
    public $table = "standartquestion";
    public $fillable = ['test_id_fk','questiontext'];

    public $timestamps = false;
}
