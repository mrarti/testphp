<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index(){
        return view('pages.welcome');
    }

    public function about(){
        return view('pages.about');
    }

    public function db(){
        $test = \DB::select('select * from tests');
        return view('pages.welcome',["test"=>$test]);
    }
}
