<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use App\Test;
use Validator;

class AddTestController extends Controller
{
    public function addView()
    {
        return view('pages.add');
    }

    public function questView()
    {
        $idquest = \DB::table('tests')->orderByRaw('test_id_pk DESC')->first();
        $quest = \DB::table('standartquestion')->where('test_id_fk','=',$idquest->test_id_pk)->get();
        return view('pages.quest',["quest"=>$quest, "idquest"=>$idquest]);
    }

    public function questPost(Request $request)
    {
        // dd($request->all());

        $rules = [];


        foreach($request->input('answertext') as $key => $value) {
            $rules["answertext.{$key}"] = 'required';
        }


        $validator = Validator::make($request->all(), $rules);


        if ($validator->passes()) {

            $idquest = \DB::table('tests')->orderByRaw('test_id_pk DESC')->first();
            $quest = \DB::table('standartquestion')->where('test_id_fk','=',$idquest->test_id_pk)->get();


            foreach($quest as $question){
                foreach($request->input('questiontext') as $key => $value) {
                    \DB::table('answer')->insert(
                        ['standartquestion_id_fk'=>$question->standartquestion_id_pk,'questiontext'=>$value]
                    );

                }
            }
            return response()->json(['success'=>'done']);
        }
        return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function addPost(Request $request){
        $rules = [];


        foreach($request->input('questiontext') as $key => $value) {
            $rules["questiontext.{$key}"] = 'required';
        }


        $validator = Validator::make($request->all(), $rules);


        if ($validator->passes()) {
            
            \DB::table('tests')->insert(
                ['explanation' =>$request->get('nametest')]
            );

            $idtest = \DB::table('tests')
                        ->orderByRaw('test_id_pk DESC')
                        ->first();


            foreach($request->input('questiontext') as $key => $value) {
                \DB::table('standartquestion')->insert(
                    ['test_id_fk'=>$idtest->test_id_pk,'questiontext'=>$value]
                );

            }
            return response()->json(['success'=>'done']);
        }
        return response()->json(['error'=>$validator->errors()->all()]);
    }


}
