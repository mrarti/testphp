<!DOCTYPE html>
<html>
<head>
    <title>Test PHP - @yield('title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">  
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="{{URL::to('css/bootstrap.min.css')}}" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
  <body>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
  <nav class="navbar navbar-expand-lg navbar-light bg-light" style="margin-bottom:20px;">
  <a class="navbar-brand" href="{{ url('/') }}">
    <img src="/img/signcheck.png" width="30" height="30" class="d-inline-block align-top" alt="">
    PHP Test
  </a>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
  <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="{{ url('/about') }}">О сайте @section('sidebar') @show</a>
      </li>
      @if (Auth::guest())
      <li class="nav-item active">
        <a class="nav-link" href="{{ url('/login') }}">Войти<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="{{ url('/register') }}">Регистрация<span class="sr-only">(current)</span></a>
      </li>
      @else
      <li class="dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          {{ Auth::user()->name }} <span class="caret"></span>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" role="menu">
          <a class="dropdown-item"  href="{{URL::to('/test')}}">Добавить тест</a>
          <a class="dropdown-item" href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выход </a>
            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
      </ul>
      </li>
         @endif
    </ul>
  </div>
</nav>
<div class="container">
    @yield('maincontent')
</div>
</div>
</div>
  </body>
  <footer style="padding-top: 80px;"><div class="fixed-bottom p-3 bg-success text-white">Nikita Andreev RDIR51 2017</div></footer>
</html>