@extends('layouts.app')
@section('title','Добавление теста')
@section('maincontent')
<form name="add_name" id="add_name">  
<h1>Создание новго теста</h1>
<div class="form-group">
  <label for="nametest">Название теста</label>
  <input type="text" class="form-control" name="nametest" id="nametest" placeholder="Имя теста">
</div>
<h2>Добавление вопросов</h2>
  <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Введите вопрос</label>
  <div class="alert alert-danger print-error-msg" style="display:none">
  <ul></ul>
  </div>
  <div class="alert alert-success print-success-msg" style="display:none">
  <ul></ul>
  </div>
  <div class="table-responsive">  
      <table class="table table-bordered" id="dynamic_field">  
          <tr>  
              <td><input type="text" name="questiontext[]" placeholder="Введите вопрос №1" class="form-control name_list" /></td>  
              <td><button type="button" name="add" id="add" class="btn btn-success">Добавить вопрос</button></td>  
          </tr>  
      </table>  
      <input type="button" name="submit" id="submit" class="btn btn-info" value="Далее" />  
  </div>
</div>
</form>
<script type="text/javascript">
    $(document).ready(function(){      
      var postURL = "<?php echo url('test'); ?>";
      var postURLQuest = "<?php echo url('quest'); ?>";
      var i=1;  


      $('#add').click(function(){  
           i++;  
           $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added"><td><input type="text" name="questiontext[]" placeholder="Введите вопрос №'+i+'" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">Удалить вопрос</button></td></tr>');  
      });  


      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  


      $.ajaxSetup({
        beforeSend: function(xhr, type) {
            if (!type.crossDomain) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
            }
        },
    });


      $('#submit').click(function(){            
           $.ajax({  
                url:postURL,  
                method:"POST",  
                data:$('#add_name').serialize(),
                type:'json',
                success:function(data)  
                {
                    if(data.error){
                        printErrorMsg(data.error);
                    }else{
                        i=1;
                        $('.dynamic-added').remove();
                        $('#add_name')[0].reset();
                        $(".print-success-msg").find("ul").html('');
                        $(".print-success-msg").css('display','block');
                        $(".print-error-msg").css('display','none');
                        $(".print-success-msg").find("ul").append('<li>Запись была добавлена</li>');
                        document.location.href = postURLQuest;
                    }
                }  
           });  
      });  


      function printErrorMsg (msg) {
         $(".print-error-msg").find("ul").html('');
         $(".print-error-msg").css('display','block');
         $(".print-success-msg").css('display','none');
         $.each( msg, function( key, value ) {
            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
         });
      }
    });  
</script>
@endsection